var gulp = require('gulp');

var minifyCss = require('gulp-minify-css');

var coffee = require('gulp-coffee');

var sass = require('gulp-sass');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

var notify = require('gulp-notify');

var browserSync = require('browser-sync');

var reload = browserSync.reload;

var connectPHP = require('gulp-connect-php');


// ////////////////////////////////////////////////
// Paths to source files
// paths haves changed a bit.
// ///////////////////////////////////////////////

var paths = {
  html: ['./app/**/*.php'],
  css: ['./app/styles/**/*.scss'],
  script: ['./app/js/**/*.js']
};


gulp.task('mincss', function () {

  var processors = [
    autoprefixer,
    // flexibility // not working properly
  ];

  return gulp.src(paths.css)

    .pipe(sass().on('error', sass.logError))

    .pipe(postcss(processors))

    // .pipe(minifyCss())

    .pipe(gulp.dest('app/css'))

    .pipe(reload({ stream: true }));

});


// ////////////////////////////////////////////////
// HTML task
// ///////////////////////////////////////////////

gulp.task('html', function () {
  gulp.src(paths.html)
    .pipe(reload({ stream: true }));
});


// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////

gulp.task('browserSync', function () {
  browserSync({
    injectChanges: true,
    proxy: '127.0.0.1/alchemist/app',
    port: 8080
  });
});


// /////////////////////////////////////////////////
// PHP task
// ////////////////////////////////////////////////

gulp.task('php', function () {
  connectPHP.server({ base: './', keepalive: true, hostname: 'localhost', port: 8080, open: false });
});

// Note: no es6 to compile
gulp.task('scripts', function () {

  return gulp.src(paths.script)

    // .pipe(coffee())
    // .pipe(gulp.dest('js'))

    .pipe(reload({ stream: true }));

});


gulp.task('watcher', function () {

  gulp.watch(paths.css, ['mincss']);

  // Note: no es6 to compile
  // gulp.watch(paths.script, ['scripts']);
  gulp.watch(paths.script, ['scripts']);

  gulp.watch(paths.html, ['html']);

});


// gulp.task('default', ['watcher', 'browserSync', 'php']);
gulp.task('default', ['watcher', 'browserSync']);
