<?php
require_once "libs/phpmailer/PHPMailerAutoload.php";
//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;
$mail->Username = "alchemist@rabbitstale.com"; // Alchemist - sender email here
$mail->Password = "rabbitjump1"; // Alchemist - password here
//Set who the message is to be sent from
$mail->setFrom('alchemist@rabbitstale.com', 'Alchemist Contact');
//Set an alternative reply-to address
// $mail->addReplyTo('tao.chainut@rabbitstale.com', 'Tao Chainut');
// Alchemist - Set who the message is to be sent to
$mail->addAddress('tao.chainut@rabbitstale.com', 'Tao Chainut');
//Set the subject line
$mail->Subject = 'Alchemist Contact';
$mail->Body =
  "full name: {$_POST['fullname']}<br/>".
  "email: {$_POST['email']}<br/>".
  "phone: {$_POST['phone']}<br/>".
  "message: {$_POST['message']}<br/>"
;
$mail->AltBody = 'This is a plain-text message body';
if (!$mail->send()) {
  error($mail->ErrorInfo);
} else {
  success('success');
}

function error($message) {
  header('HTTP/1.1 500 Internal Server Error');
  header('Content-Type: application/json; charset=UTF-8');
  die(json_encode(array('message' => $message, 'code' => 500)));
}

function success($message) {
  header('Content-Type: application/json');
  print json_encode(['message' => $message]);
}
