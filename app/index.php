<?php
$asset_path = "./";
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="Alchemist - Successful Data Activation">
  <title>Alchemist | Successful Data Activation</title>
  <link rel="canonical" href="http://alchemist.artplore.com"/>
  <link rel="stylesheet" type="text/css" media="all"
        href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet"/>
</head>
<body>

<div id="main-nav">
  <div class="nav-inner">
    <div id="nav-toggle"><span></span></div>
    <div class="nav-logo">
      <img src="./imgs/logo-mini.png"/>
    </div>
    <div class="nav-list">
      <a href="#sec-home" class="nav-item">Home</a>
      <a href="#sec-about" class="nav-item">About</a>
      <a href="#sec-work" class="nav-item">Our Work</a>
      <a href="#sec-contact" class="nav-item nav-contact">Contact us</a>
    </div>
  </div>
</div>

<!-- Home -->
<div id="sec-home" class="layout-outer">
  <div class="layout-inner flex-center">
    <img class="_logo" src="./imgs/logo.png"/>
    <div class="_scroll flex-v-center">

      <!-- Note: for using original design mouse img -->
      <!-- <img class="__mouse" src="./imgs/icon_mouse.png"/>-->

      <div class="__mouse"></div>
      <div class="__instruction">Scroll</div>
      <div class="__line"></div>
    </div>
  </div>
</div>

<!-- About -->
<div id="sec-about" class="layout-outer layout-1">
  <div class="layout-inner-full">
    <div class="_left"></div>
    <div class="_right">
      <img class="__logo" src="./imgs/logo-light.png"/>
      <div class="__content">
        <span class="highlight">Alchemist</span> is the latest subsidiary of Rabbit Digital Group,
        focusing on developing and implementing Data Activation,
        a concept of using data to create seamless experiences
        through all digital platforms.
        <br/><br/>
        We are a well-balanced, energetic team of business
        consultants, software developers, and creative people. With our diverse skill sets and experience of working
        with
        various A-list clients, we are able to tackle business
        challenges at holistic views, and provide end-to-end data solutions from business consulting to developing
        data-driven products.
      </div>
    </div>
  </div>
</div>

<!-- Data Activation -->
<div id="sec-work">
  <div class="data-act">
    <div class="divider"></div>
    <h1>DATA ACTIVATION</h1>
    <div class="layout-inner-full">
      <div class="_left"></div>
      <div class="_right">
        Unifying and using customer data to
        create end-to-end experience
        through all digital channels
      </div>
    </div>
  </div>
  <!-- Info Graphic -->
  <div class="layout-outer sec-info">
    <div class="layout-inner">
      <img class="_info-img hide-md" src="./imgs/sec-info.jpg"/>
      <img class="_info-img show-md" src="./imgs/sec-info-mobile.jpg"/>
      <div class="_info-desc">
        <h2 class="show-md">DATA IN</h2>
        <ul class="outer-ul">
          <li><span>Bringing in data from different sources, platforms, and formats</span></li>
          <li><span>Unifying all data into centralized platform</span></li>
        </ul>
        <h2 class="show-md">Value unlocked</h2>
        <ul class="outer-ul">
          <li><span>Data Integration & transformation</span></li>
          <li><span>Data Analytics</span></li>
          <li><span>Data Protection & privacy setting</span></li>
        </ul>
        <h2 class="show-md">Impact out</h2>
        <ul class="outer-ul">
          <li><span>Developing data-driven products</span>
            <ul class="inner-ul">
              <li><span>- App-in-App</span></li>
              <li><span>- One-to-One Automation</span></li>
              <li><span>- Chatbot / IOT</span></li>
              <li><span>- Ad optimization</span></li>
              <li><span>- Business Intelligence</span></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Venn Diagram -->
  <div class="sec-diagram">
    <div class="divider hide-md"></div>
    <div class="diagram">
      <h1>
        Data Activation<br/>
        is not just about<br class="show-md"/>
        using tools
      </h1>
    </div>
  </div>
</div>


<!-- Outro -->
<div class="sec-outro">
  <div class="divider"></div>
  <div class="_wrap">
    <div class="_inner">
      <div class="outro">
        <div class="_text">
          <h2>Successful Data Activation</h2>
          <p>is not about how much data you have<br/>
            It’s more about relevancy of data,<br class="show-md"/> and how to use it</p>
          <h2 class="_header-2">
            Data strategy <br class="show-md"/>
            is a long-shot game.
          </h2>
          <p>It is not just an expense.<br/>
            It is an investment.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Contact Address -->
<div id="sec-contact">
  <div class="divider"></div>
  <!-- Address -->
  <div class="layout-outer sec-address">
    <div class="layout-inner-full">
      <div class="_left">
        <div class="map-responsive">
          <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.653105368925!2d100.51958821516983!3d13.739439801280712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e299169d8d9091%3A0x2b7b4bafca6a8431!2sRabbit&#39;s+Tale+%7C+digital+creative+agency!5e0!3m2!1sen!2sth!4v1541341817982"
              width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="_right flex-center">
        <div class="flex-v-center">
          <img class="__logo" src="./imgs/logo-light.png"/>
          <div>
            <div class="__address">
              1706/34 ,Rama VI Road,<br/>
              Rongmuang, Pathumwan,<br/>
              Bangkok 10330
            </div>
            <div class="__number">
              Tel 02-219-2385-7
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Contact Form -->
  <div class="layout-outer sec-contact">
    <div class="layout-inner">
      <h1>Contact Us</h1>

      <!-- Form -->
      <form id="contact-form" class="contact-form flex-v-center" method="POST">
        <div class="form-items">
          <div class="_left">
            <div class="form-item">
              <input class="ts-input" type="text" name="fullname" id="fullname" placeholder="Full Name"/>
            </div>
            <div class="form-item">
              <input class="ts-input" type="text" name="email" id="email" placeholder="Email"/>
            </div>
            <div class="form-item">
              <input class="ts-input" type="text" name="phone" id="phone" placeholder="Telephone Number"/>
            </div>
          </div>
          <div class="_right">
            <div class="form-item">
              <textarea class="ts-input" name="message" id="message" placeholder="Message"></textarea>
            </div>
          </div>
        </div>
        <div class="btn-wrap">
          <button type="reset" class="btn btn-reset">Reset</button>
          <br><br>
          <button type="submit" class="btn btn-primary btn-send">Send</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- Footer -->
<div id="footer">
  <div class="_top">
    <div class="_inner">
      <div class="__main-logo">
        <a href="http://www.rabbitdigitalgroup.com/" target=_blank><img src="./imgs/logo-f-rabbit.png"/></a>
      </div>
      <div class="_logo-wrap">
        <a href="http://www.rabbitstale.com/" target=_blank><img class="__rabbit" src="./imgs/logo-fm-rabbit.png"/></a>
        <a href="https://moonshot.co.th/" target=_blank><img class="__moonshot" src="./imgs/logo-fm-moonshot.png"/></a>
        <a href="#"><img class="__cnc" src="./imgs/logo-fm-cnc.png"/></a>
        <a href="#"><img class="__zero" src="./imgs/logo-fm-zero.png"/></a>
        <a href="#"><img class="__alchemist" src="./imgs/logo-fm-alchemist.png"/></a>
      </div>
    </div>
  </div>
  <div class="_bottom layout-outer">
    <div class="_inner">
      <div><img class="__logo" src="./imgs/logo-footer.png"/></div>
      <div class="__text">A Subsidiary of Rabbit Digital Group.</div>
      <div class="__copy">Copyrights © 2018 All Rights Reserved</div>
    </div>
  </div>
</div>

<!-- Popup -->
<div id="popup-thanks" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-inner small">
    <div class="sp-close close-popup">×</div>
    <!-- Begin Content -->
    <!-- Note: the class `step2` is for switching to newsletters selection -->
    <div class="mp-content centered">
      <h1 class="heading2">We have successfully received your message</h1>
      <h3 class="heading3">Our team will contact you shortly.
      </h3>
      <div class="btn btn-primary btn-close close-popup">Close</div>
    </div>
    <!-- End Content -->

  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="./js/index.js?<?php echo time(); ?>"></script>
<script src="./js/slideNav.js?<?php echo time(); ?>"></script>
<script src="./js/popup.js?<?php echo time(); ?>"></script>
</body>
</html>


