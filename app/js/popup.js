function initAllPopups() {
  // Note: This only connects popup to the triggers.
  popup('popup-thanks', 'pop-thanks');
}
function popup(modalId, triggerClass) {
  var modal = document.getElementById(modalId);
  var closes = document.getElementsByClassName('close-popup');
  var triggers = document.getElementsByClassName(triggerClass);

  // When the user clicks on the button, open the modal
  for (var i in triggers) {
    triggers[i].onclick = function () {
      modal.style.display = "block";
      document.body.classList.add('body-no-scroll');
    }
  }
  // fix for iPhone iPad
  for (i in closes) {
    closes[i].onclick = function () {
      $('.ml-popup').css('display', 'none');
      // modal.style.display = "none";
      document.body.classList.remove('body-no-scroll');
    }
  }
}
function openPopupById(modalId) {
  var modal = document.getElementById(modalId);
  modal.style.display = "block";
  document.body.classList.add('body-no-scroll');
}
