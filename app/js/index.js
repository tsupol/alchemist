$(document).ready(function(){

  // Popups
  // ----------------------------------------
  initAllPopups();

  // Slide
  // ----------------------------------------
  window.slide = new SlideNav();
  var nav = new SlideNav({
    activeClass: "active",
    toggleButtonSelector: true,
    changeHash: true,
  });

  // menu
  // ----------------------------------------
  $('#nav-toggle').click(function () {
    $('body').toggleClass('mobile-menu-expanded');
  });
  $('#main-nav .nav-item').click(function () {
    $('body').removeClass('mobile-menu-expanded');
  });

  // Send Email
  // ----------------------------------------
  var request;
  $('#contact-form').validate({
    // rules: {
    //   email: {
    //     required: true,
    //   },
    // },
    submitHandler: function(form) {
      // Abort any pending request
      if (request) {
        request.abort();
      }
      // setup some local variables
      var $form = $(form);

      // Let's select and cache all the fields
      var $inputs = $form.find("input, select, button, textarea");

      // Serialize the data in the form
      var serializedData = $form.serialize();

      $inputs.prop("disabled", true);

      // Fire off the request to /form.php
      request = $.ajax({
        url: "./gmail.php",
        type: "post",
        data: serializedData,
        timeout: 15000
      });

      // Callback handler that will be called on success
      request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        $('.ts-input').val('');
        openPopupById("popup-thanks");
      });

      // Callback handler that will be called on failure
      request.fail(function (jqXHR, textStatus, errorThrown){
        alert("Something went wrong. Please try again in 10 minutes.");
        // Log the error to the console
        console.error(
          "The following error occurred: "+
          textStatus, errorThrown
        );
      });

      // Callback handler that will be called regardless
      // if the request failed or succeeded
      request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
      });
    }
  });

});
